import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, Button } from 'react-native';
import ImagePicker from 'react-native-image-picker';
import firebase from './src/FirebaseConnection';
import RNFetchBlob from 'react-native-fetch-blob';

window.XMLHttpRequest = RNFetchBlob.polyfill.XMLHttpRequest;
window.Blob = RNFetchBlob.polyfill.Blob;

export default class FirebaseStorage extends Component {

  constructor(props) {
    super(props);
    this.state = {
      foto:null,
      pct:0,
      upSlit:0,
      upTotal:0
    };

    this.pegarFoto = this.pegarFoto.bind(this);
    this.carregarAvatar = this.carregarAvatar.bind(this);
    this.remover = this.remover.bind(this);

    this.carregarAvatar('images/image2.jpg');
  }

  carregarAvatar(img) {
    firebase.storage().ref().child(img).getDownloadURL().then((url)=>{
      let state = this.state;
      state.foto = {uri:url};
      this.setState(state);
    })
  }

  pegarFoto() {
    let options = {
      title:'Selecionar uma foto'
    }

    ImagePicker.launchImageLibrary(options, (r)=>{
      if(r.uri) {
        let uri = r.uri.replace('file://', '');

        let imagem = firebase.storage().ref().child('images').child('image.jpg');
        let mime = 'image/jpeg';

        RNFetchBlob.fs.readFile(uri, 'base64')
          .then((data)=>{
            return RNFetchBlob.polyfill.Blob.build(data, {type:mime+';BASE64'});
          })
          .then((blob)=>{
            imagem.put(blob, {contentType:mime})
              .on('state_changed', (snapshot)=>{
                
                let pct = Math.floor(( snapshot.bytesTransferred / snapshot.totalBytes ) * 100);
                let state = this.state;
                state.pct = pct;
                state.upSlit = snapshot.bytesTransferred;
                state.upTotal = snapshot.totalBytes;
                this.setState(state);

              }, (error)=>{
                alert(error.code);
              }, ()=>{
                imagem.getDownloadURL().then((url)=>{
                  let state = this.state;
                  state.foto = {uri:url};
                  this.setState(state);
                });
              })

          });

      }
    });
  }

  remover() {
    firebase.storage().ref().child('images/image.jpg').delete().then(()=>{
      this.setState({foto:null});
    })
  }

  render() {
    return(
      <View style={styles.container}>
        <Button title="Pegar photo" onPress={this.pegarFoto} />
        <Text>{this.state.pct}%</Text>
        <View style={{ width:this.state.pct+'%', height:40, backgroundColor:'red' }}><Text>{this.state.upSlit} / {this.state.upTotal}</Text></View>
        <Image source={this.state.foto} style={styles.foto} />
        <Button title="Remover" onPress={this.remover} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container:{
    flex:1,
    justifyContent:'center',
    alignItems:'center'
  },
  foto:{
    width:300,
    height:300,
    borderRadius:150
  }
});